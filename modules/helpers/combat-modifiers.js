//###########################################################
/** Combat Modifier  */
/**Active Mods */
/*  active Mods will be all modifiers affecting rolls.*/
/* 
List of finished Active Mods
- Base steps or Attributes
- Ranks
- Wound modifier
- Bonus to all rolls modifier
- Bonus to all close Combat Attacks
- Situation Modifier - Knockdown
- Situation Modifier - Harried
- Combat Modifier - Defensive Stance
- Combat Modifier - Aggressive Stance
*/
/*
List of Combat Modifiers to be implemented
- Situation Modifier - Blindsided
- Situation Modifier - Cover
- Situation Modifier - Darkness
- Situation Modifier - Dazzled
- Situation Modifier - Overwhelmed
- Situation Modifier - Impaired Movement
- Situation Modifier - Range
- Situation Modifier - Surprised
- Combat Modifier - Attacking to Knockdown
- #311 - Combat Modifier - Attacking to stun 
- Combat Modifier - called shot
- Combat Modifier - Jump Up --> function is included. Active mods + chat are missing
- Combat Modifier - Setting against a charge
- Combat Modifier - Shattering a shield
- Combat Modifier - splitting Movement
- Combat Modifier - Tail attack
- Low Strength / low dex Modifier
- Bonus to all damage of close Attacks
- Bonus to all ranged attacks
- Bonus to all damage of ranged attacks
- Bonus to all damage
- Bonus to all attacks
- etc.
*/
//###########################################################

export function defenseModifiers(actor) {
  let mod = 0;
  /* a knocked down character cannot use any combat options with the exception of jump up */
  if (actor.system.tactics.knockeddown === true) {
    mod -= 3;
  } else {
    if (actor.system.tactics.aggressive === true) {
      mod -= 3;
    } else if (actor.system.tactics.defensive === true) {
      mod += 3;
    }
  }

  if (actor.system.tactics.harried === true) {
    mod -= 2;
  }

  return mod;
}

export function actionTestModifiers(actor, rolltype, closeCombat, attackType) {
  let mod = 0;
  let strain = 0;
  /* a knocked down character cannot use any combat options with the exception of jump up */
  //###########################################################
  /** Jump up  */
  //###########################################################
  if (actor.system.tactics.knockeddown === true && rolltype !== 'jumpUp') {
    mod -= 3;
  } else {
    //###########################################################
    /** Defensive Stance  */
    //###########################################################
    if (actor.system.tactics.defensive && rolltype !== 'knockdown') {
      mod -= 3;
    }
    //###########################################################
    /** aggressive Stance  */
    //###########################################################
    else if (actor.system.tactics.aggressive === true && rolltype === 'attack') {
      if (closeCombat) {
        mod += 3;
        strain += 1;
      } else if (actor.type === 'creature' || actor.type === 'npc') {
        if (attackType !== 'rangedAttack') mod += 3;
        strain += 1;
      }
    }
  }

  //###########################################################
  /** Harried */
  //###########################################################
  if (actor.system.tactics.harried === true) {
    mod -= 2;
  }

  //###########################################################
  /** Bonuses on Attacks  */
  //###########################################################
  if (rolltype === 'attack' && closeCombat) {
    mod += actor.system.bonuses.closeAttack + actor.system.bonuses.allAttack;
  }
  //###########################################################
  /** Bonuses on Ranged Attack  */
  //###########################################################
  else if (rolltype === 'attack' && !closeCombat && actor.type != 'creature') {
    mod += actor.system.bonuses.rangedAttack + actor.system.bonuses.allAttack;
  }

  return { modifiers: mod, strain: strain };
}

export function damageTestModifiers(actor, rolltype) {
  let mod = 0;
  console.log('ROLLTYPE INSIDE DAMAGETESTMOD ' + rolltype);

  //###########################################################
  /** Damage bonus Aggressive Attack */
  //###########################################################

  if (actor.system.tactics.aggressive === true && rolltype !== 'recovery') {
    if (rolltype === 'weapondamage' || rolltype === 'attackdamage') {
      mod += 3;
      console.log('ROLLTYPE: ' + rolltype);
    } else {
      mod = 0;
    }
  }

  //###########################################################
  /** Damage Malus Defensive Stance */
  //###########################################################
  else if (actor.system.tactics.defensive && rolltype !== 'recovery') {
    mod -= 3;
  }

  if (actor.system.wounds > 0 && rolltype !== 'recovery') {
    mod -= actor.system.wounds;
  }

  return mod;
}

export function initiativeMod(actor) {
  let mod = 0;
  if (actor.system.tactics.harried === true) {
    mod -= 2;
  }
  if (actor.system.tactics.defensive) {
    mod -= 3;
  }
  if (actor.system.tactics.knockeddown) {
    mod -= 3;
  }
  return mod;
}

export function getTargetDifficulty(defenseType, groupDefenseType) {
  let currentTarget = game.user.targets.first()?.actor;
  let currentTargets = [...game.user.targets.map((t) => t.actor)];
  let numTargets = game.user.targets.size;

  if (numTargets <= 0 || !defenseType) {
    return 0;
  } else {
    let difficulty = 0;
    let baseDifficulty = 0;
    let additionalTargetDifficulty = 0;

    if (groupDefenseType) {
      switch (groupDefenseType) {
        case 'defenseHighPlus':
          additionalTargetDifficulty = numTargets - 1;
        case 'defenseHigh':
          baseDifficulty = _getAggregatedDefense(currentTargets, defenseType, Math.max);
          break;
        case 'defenseLowPlus':
          additionalTargetDifficulty = numTargets - 1;
        case 'defenseLow':
          baseDifficulty = _getAggregatedDefense(currentTargets, defenseType, Math.min);
          break;
      }
      difficulty = baseDifficulty + additionalTargetDifficulty;
    } else {
      difficulty = currentTarget?.getModifiedDefense(defenseType) ?? 0;
    }

    return difficulty;
  }

  /*  const targets = Array.from(game.user.targets);
  var defense = 0;
  if (targets.length > 0) {
    defense = targets[0].actor.system[defenseType];
    if (targets[0].actor.type !== 'pc') {
      defense += defenseModifiers(targets[0].actor);
    }
  }
  return defense;*/
}

function _getAggregatedDefense(targets, targetDefenseType, aggregate = Math.max) {
  return targets.length > 0 ? aggregate(...targets.map((t) => t.getModifiedDefense(targetDefenseType))) : 0;
}
