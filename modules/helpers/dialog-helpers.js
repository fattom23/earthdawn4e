export function confirmDeletionDialog() {
  return new Promise((resolve) => {
    Dialog.confirm({
      title: game.i18n.localize('earthdawn.d.deleteAllLPrecords'),
      content: game.i18n.localize('earthdawn.warns.warnDeleteAllReallySure'),
      yes: () => resolve(true),
      no: () => resolve(false),
      defaultYes: false,
    });
  });
}
