export default class earthdawn4eItem extends Item {
  // CLEAN-UP = this function is called three times if the item is owned. This should be figured out and cleaned up
  prepareData() {
    super.prepareData();
    const itemData = this;
    if (this.type === 'spellmatrix') {
      this._prepareItemData(itemData);
    } else if (this.type === 'thread' && this.isEmbedded === true) {
      this._prepareThreadData(itemData);
    } else if (this.type === 'weapon' || this.type === 'armor') {
      this._prepareForgedItems(itemData);
    }
  }

  _prepareItemData(itemData) {
    const systemData = itemData.system;
    systemData.totalthreads = this.threadCount();
    systemData.totalthreadsneeded = Number(systemData.threadsrequired) + Number(systemData.targetadditionalthreads);
  }
  _prepareThreadData(itemData) {}
  threadCount() {
    let totalThreads;
    totalThreads = Number(this.system.constantthreads) + Number(this.system.activethreads);
    return totalThreads;
  }

  _prepareForgedItems(itemData) {
    const systemData = itemData.system;
    if (itemData.type === 'weapon') {
      systemData.damageFinal = Number(systemData.damagestep) + Number(systemData.timesForged);
      systemData.damageTotal = this.getFinalDamage();
    } else {
      systemData.physicalArmorFinal = Number(systemData.Aphysicalarmor) + Number(systemData.timesForgedPhysical);
      systemData.mysticArmorFinal = Number(systemData.Amysticarmor) + Number(systemData.timesForgedMystic);
    }
  }

  getFinalDamage() {
      if(this.isOwned){
        let actor = game.actors.get(this.parent._id);
        let attribute = ""
        let attributeStep = 0
        if (actor.type == "pc" ) {
          attribute = this.system.damageattribute + 'value';
          attributeStep = actor.getStep(actor.system.attributes[attribute]);
        } else {
          attribute = this.system.damageattribute + 'Step';
          attributeStep = actor.system[attribute];
        }
        
        let finalDamage = Number(attributeStep) + this.system.damageFinal;
        return finalDamage;
      } else {
        let finalDamage = this.system.damageFinal;     
        return finalDamage;
      }
      
  }
}
